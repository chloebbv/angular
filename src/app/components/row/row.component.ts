import { Component, OnInit, Input } from '@angular/core';
import { Serie } from 'src/app/models/serie.model';
import { SeriesService } from 'src/app/services/series/series.service';

@Component({
  selector: '[app-row]',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.css']
})
export class RowComponent implements OnInit {
  @Input() id;
  @Input() name;
  @Input() date;
  serie: Serie;
  commentsDisabled: boolean = false;
  detailsUrl: string;
  editUrl: string;
  commentsUrl: string;

  constructor(private seriesService: SeriesService) {
  }

  ngOnInit(): void {
    //Define custom paths with the serie's id
    this.detailsUrl = '/serie/' + this.id;
    this.editUrl = '/serie/edit/' + this.id;
    this.commentsUrl = '/serie/' + this.id + '/comments';
    //Retrieve serie from his id
    this.serie = this.seriesService.getElementById(this.id);
    //Check if serie has comments
    //If not => disabled show comments button
    if (this.serie.comments.length === 0) {
      this.commentsDisabled = true;
    }
  }


  deleteSerie() {
    //Open confirmation dialog and to confirm delete
    const deleteConfirmation = confirm('Voulez-vous vraiment supprimer la série ' + this.serie.name + '?');
    //If deleteConfirmation = true ->
    //Call the SeriesService's method to delete a serie from his id
    if (deleteConfirmation) {
      this.seriesService.deleteSerie(+this.id);
    }
  }


}

