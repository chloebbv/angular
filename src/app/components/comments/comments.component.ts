import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SeriesService } from 'src/app/services/series/series.service';
import { Serie } from 'src/app/models/serie.model';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  @Input() id;
  @Input() date;
  @Input() author;
  @Input() content;
  @Input() idSerie;
  @Output() isHiddenEmit: EventEmitter<any> = new EventEmitter<any>();
  serie: Serie;
  isHidden: boolean = false;

  constructor(private seriesService: SeriesService) { }

  ngOnInit(): void {
    //Retrieve the serie from id 
    this.serie = this.seriesService.getElementById(+this.idSerie);
  }



  //Call the SeriesService's method to delete a comment from his id
  deleteComment() {
    //Open confirmation dialog to confirm delete
    const deleteConfirmation = confirm('Voulez-vous vraiment supprimer le commentaire de ' + this.author + '?');
    //If deleteConfirmation = true 
    if (deleteConfirmation) {
      //Call the SeriesService's method to delete a serie from his id
      this.seriesService.deleteComment(+this.serie.id, +this.id);
      //If serie.comments.length = 0 send true to parent component 
      //to hide sort button with ngIf
      if (this.serie.comments.length === 0) {
        this.isHidden = true;
        this.isHiddenEmit.emit(this.isHidden);
      }
      //If serie.comments.length != 0 send false to parent component 
      //to display sort button with ngIf
      else {
        this.isHidden = false;
        this.isHiddenEmit.emit(this.isHidden);
      }
    }

  }

}
