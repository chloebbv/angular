import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackToSeriesComponent } from './back-to-series.component';

describe('BackToSeriesComponent', () => {
  let component: BackToSeriesComponent;
  let fixture: ComponentFixture<BackToSeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackToSeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackToSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
