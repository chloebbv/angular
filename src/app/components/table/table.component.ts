import { Component, OnInit, OnDestroy } from '@angular/core';
import { Serie } from 'src/app/models/serie.model';
import { Subscription } from 'rxjs';
import { SeriesService } from 'src/app/services/series/series.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  series: Array<Serie>;
  seriesSubscritpion: Subscription;

  constructor(private seriesService: SeriesService) { }

  //Subscribe to the observer 
  //Update this.series with _series (of SeriesService) values  
  ngOnInit(): void {
    this.seriesSubscritpion = this.seriesService.seriesSubject.subscribe(
      (series: Array<Serie>) => {
        this.series = series;
      }
    );
    this.seriesService.emitSeriesSubject();
  }

  //Unsubscribe observer
  ngOnDestroy(): void {
    this.seriesSubscritpion.unsubscribe();
  }

}
