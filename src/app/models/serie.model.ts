import { Comment } from 'src/app/models/comment.model';

export class Serie {
    //Declare attributes
    private _id: number;
    private _name: string;
    private _firstSeasonReleaseDate: Date;
    private _numberOfSeasons: number;
    private _description: string;
    private _critique: string;
    private _image: string;
    private _comments: Array<Comment>;
    //Use static count to autoincrement id
    static count: number = 0;

    //Constructor
    constructor(name: string, firstSeasonReleaseDate: Date, numberOfSeasons: number,
        description: string, critique: string, image: string) {
        this._id = ++Serie.count;
        this._name = name;
        this._firstSeasonReleaseDate = firstSeasonReleaseDate;
        this._numberOfSeasons = numberOfSeasons;
        this._description = description;
        this._critique = critique;
        this._image = image;
        this._comments = new Array<Comment>();
    }

    //Getters and setters
    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }
    public get firstSeasonReleaseDate(): Date {
        return this._firstSeasonReleaseDate;
    }
    public set firstSeasonReleaseDate(value: Date) {
        this._firstSeasonReleaseDate = value;
    }
    public get numberOfSeasons(): number {
        return this._numberOfSeasons;
    }
    public set numberOfSeasons(value: number) {
        this._numberOfSeasons = value;
    }
    public get description(): string {
        return this._description;
    }
    public set description(value: string) {
        this._description = value;
    }
    public get critique(): string {
        return this._critique;
    }
    public set critique(value: string) {
        this._critique = value;
    }
    public get image(): string {
        return this._image;
    }
    public set image(value: string) {
        this._image = value;
    }
    public get comments(): Array<Comment> {
        return this._comments;
    }
    public set comments(value: Array<Comment>) {
        this._comments = value;
    }

}