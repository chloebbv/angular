export class User {
    //Declare attributes
    private _username: string;
    private _password: string;

    //Constructor
    constructor(public usernameStr, public passwordStr) {
        this._username = usernameStr;
        this._password = passwordStr;
    }

    //Getters and setters
    public get username(): string {
        return this._username;
    }
    public set username(value: string) {
        this._username = value;
    }

    public get password(): string {
        return this._password;
    }
    public set password(value: string) {
        this._password = value;
    }


}