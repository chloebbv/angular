export class Comment {
    //Declare attributes
    private _id: number;
    private _date: Date;
    private _author: string;
    private _content: string;
    //Use static count to autoincrement id
    static count: number = 0;

    //Constructor
    constructor(date: Date, author: string, content: string) {
        this._id = ++Comment.count;
        this._date = date;
        this._author = author;
        this._content = content;
    }

    //Getters and setters 
    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
    public get date(): Date {
        return this._date;
    }
    public set date(value: Date) {
        this._date = value;
    }
    public get author(): string {
        return this._author;
    }
    public set author(value: string) {
        this._author = value;
    }
    public get content(): string {
        return this._content;
    }
    public set content(value: string) {
        this._content = value;
    }

}