import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SeriesService } from './services/series/series.service';
import { AuthService } from './services/auth/auth.service';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SeriesViewComponent } from './views/series-view/series-view.component';
import { SingleSerieViewComponent } from './views/single-serie-view/single-serie-view.component';
import { EditSerieViewComponent } from './views/edit-serie-view/edit-serie-view.component';
import { NewSerieViewComponent } from './views/new-serie-view/new-serie-view.component';
import { UserService } from './services/user/user.service';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { TableComponent } from './components/table/table.component';
import { RowComponent } from './components/row/row.component';
import { HeaderComponent } from './components/header/header.component';
import { ErrorViewComponent } from './views/error-view/error-view.component';
import { CommentsViewComponent } from './views/comments-view/comments-view.component';
import { CommentsComponent } from './components/comments/comments.component';
import { BackToSeriesComponent } from './components/back-to-series/back-to-series.component';

@NgModule({
  declarations: [
    AppComponent,
    SeriesViewComponent,
    SingleSerieViewComponent,
    EditSerieViewComponent,
    NewSerieViewComponent,
    AuthViewComponent,
    TableComponent,
    RowComponent,
    HeaderComponent,
    ErrorViewComponent,
    CommentsViewComponent,
    CommentsComponent,
    BackToSeriesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [SeriesService, AuthService, AuthGuardService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
