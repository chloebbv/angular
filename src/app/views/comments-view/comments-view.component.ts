import { Component, OnInit } from '@angular/core';
import { SeriesService } from 'src/app/services/series/series.service';
import { Serie } from 'src/app/models/serie.model';
import { Comment } from 'src/app/models/comment.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comments-view',
  templateUrl: './comments-view.component.html',
  styleUrls: ['./comments-view.component.css']
})
export class CommentsViewComponent implements OnInit {
  serie: Serie;
  detailsUrl: string;
  comments: Array<Comment>;
  sortMessage: string = 'Trier du plus ancien au plus récent';
  isHidden: boolean = false;

  constructor(private seriesService: SeriesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    //Retrieve the id parameter in path 
    const id = this.route.snapshot.params.id;
    //Retrieve the serie from id 
    this.serie = this.seriesService.getElementById(+id);
    //Define custom paths with the serie's id
    this.detailsUrl = '/serie/' + this.serie.id;
    //Initiate comments with serie.comments value
    this.comments = this.serie.comments;
    //Sort comments by date desc
    this.comments.sort((a, b) => b.date.getTime() - a.date.getTime());
  }

  //Set isHidden with boolean receive from child component 
  //to display or hide sort button with ngIf
  showButton(data) {
    this.isHidden = data;
  }

  //Sort comments array by date asc or desc
  sortByDate() {
    if (this.sortMessage === 'Trier du plus ancien au plus récent') {
      this.comments.sort((a, b) => a.date.getTime() - b.date.getTime());
      this.sortMessage = 'Trier du plus récent au plus ancien';
    } else if (this.sortMessage === 'Trier du plus récent au plus ancien') {
      this.comments.sort((a, b) => b.date.getTime() - a.date.getTime());
      this.sortMessage = 'Trier du plus ancien au plus récent';
    }
  }



}
