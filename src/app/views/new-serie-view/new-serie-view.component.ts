import { Component, OnInit } from '@angular/core';
import { SeriesService } from 'src/app/services/series/series.service';
import { Router } from '@angular/router';
import { Serie } from 'src/app/models/serie.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-new-serie-view',
  templateUrl: './new-serie-view.component.html',
  styleUrls: ['./new-serie-view.component.css']
})
export class NewSerieViewComponent implements OnInit {
  leftCharCritique: number = 500;
  leftCharDescription: number = 500;
  sCharCritique: string = 's';
  sCharDescription: string = 's';

  constructor(private seriesService: SeriesService, private router: Router) { }

  ngOnInit(): void {
  }

  //Display number of left characters for description text area
  //Delete char 's' (plural in the label) if leftChar = 1
  keyUpDescription(evt) {
    const value = evt.target.value;
    const nbOfChars = value.length;
    this.leftCharDescription = 500 - nbOfChars;
    if (this.leftCharDescription === 1) {
      this.sCharDescription = '';
    } else {
      this.sCharDescription = 's';
    }
  }

  //Display number of left characters for critique text area
  //Delete char 's' (plural in the label) if leftChar = 1
  keyUpCritique(evt) {
    const value = evt.target.value;
    const nbOfChars = value.length;
    this.leftCharCritique = 500 - nbOfChars;
    if (this.leftCharCritique === 1) {
      this.sCharCritique = '';
    } else {
      this.sCharCritique = 's';
    }
  }

  onSubmitNewSerie(form: NgForm) {
    //Retrieve values form form
    const name = form.value.serieName;
    const date = form.value.serieFirstSeasonReleaseDate;
    const nbOfSeaons = form.value.serieNumberOfSeasons;
    const description = form.value.serieDescription;
    const critique = form.value.serieCritique;
    const image = form.value.serieImage;
    //Call the SeriesService's method to create a with value retrieves from form 
    const serie: Serie = new Serie(name, date, nbOfSeaons, description, critique, image);
    this.seriesService.addSerie(serie);
    //Redirect to series-view
    this.router.navigate(['/series']);
  }

}
