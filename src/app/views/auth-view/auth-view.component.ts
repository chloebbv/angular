import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-view',
  templateUrl: './auth-view.component.html',
  styleUrls: ['./auth-view.component.css']
})
export class AuthViewComponent implements OnInit {
  authForm: FormGroup;
  errorMsg: string;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this._initForm();
  }

  _initForm() {
    this.authForm = this.formBuilder.group(
      {
        'username': ['', Validators.required],
        'password': ['', Validators.required]
      }
    )
  }

  onSignIn() {
    const username = this.authForm.value.username;
    const password = this.authForm.value.password;

    this.authService.checkAuth(username, password).then(
      (res) => {
        this.router.navigate(['series'])
      }, (err) => {
        this.errorMsg = err;
      }
    )
  }

  alertLogin() {
    alert('AHAHAH!!! Tu pensais vraiment que j\'allais te donner les identifiants comme ça!!!! 🤪');
  }

}
