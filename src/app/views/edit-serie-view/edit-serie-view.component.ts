import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Serie } from 'src/app/models/serie.model';
import { SeriesService } from 'src/app/services/series/series.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-edit-serie-view',
  templateUrl: './edit-serie-view.component.html',
  styleUrls: ['./edit-serie-view.component.css']
})
export class EditSerieViewComponent implements OnInit {
  editSerieForm: FormGroup;
  serie: Serie;
  datePipe: DatePipe = new DatePipe('en-US');
  leftCharCritique: number;
  leftCharDescription: number;
  sCharCritique: string;
  sCharDescription: string;


  constructor(private seriesService: SeriesService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    //Retrieve the id parameter in path 
    const id = this.route.snapshot.params.id;
    //Retrieve the serie from id 
    this.serie = this.seriesService.getElementById(+id);
    this._initForm();
    //Initialize number of left characters for critique text area according to serie.critique.length 
    this.leftCharCritique = 500 - this.serie.critique.length;
    //Initialize char 's' (plural in the label) according to leftCharCritique value
    this.sCharCritique = this.leftCharCritique === 1 ? '' : 's';
    //Initialize number of left characters for critique text area according to serie.description.length 
    this.leftCharDescription = 500 - this.serie.description.length;
    //Initialize char 's' (plural in the label) according to leftCharDescription value
    this.sCharDescription = this.leftCharDescription === 1 ? '' : 's';
  }

  //Initialize form values with serie's attributes 
  _initForm() {
    this.editSerieForm = this.formBuilder.group(
      {
        'serieName': [this.serie.name, [Validators.required]],
        'serieFirstSeasonReleaseDate': [this.datePipe.transform(this.serie.firstSeasonReleaseDate, 'yyyy-MM-dd'), [Validators.required]],
        'serieNumberOfSeasons': [this.serie.numberOfSeasons, [Validators.required]],
        'serieDescription': [this.serie.description, [Validators.required]],
        'serieCritique': [this.serie.critique, [Validators.required]],
        'serieImage': [this.serie.image, [Validators.required, Validators.pattern('^https?:\/\/(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:gif|Gif|GIF)$')]]
      }
    )
  }

  //Display number of left characters for description text area
  //Delete char 's' (plural in the label) if leftChar = 1
  keyUpDescription(evt) {
    const value = evt.target.value;
    const nbOfChars = value.length;
    this.leftCharDescription = 500 - nbOfChars;
    this.sCharDescription = this.leftCharDescription === 1 ? '' : 's';
  }

  //Display number of left characters for critique text area
  //Delete char 's' (plural in the label) if leftChar = 1
  keyUpCritique(evt) {
    const value = evt.target.value;
    const nbOfChars = value.length;
    this.leftCharCritique = 500 - nbOfChars;
    this.sCharCritique = this.leftCharCritique === 1 ? '' : 's';

  }

  onSubmit() {
    //Retrieve values form form
    const newName: string = this.editSerieForm.value.serieName;
    const newDate: Date = this.editSerieForm.value.serieFirstSeasonReleaseDate;
    const newNumberOfSeasons: number = this.editSerieForm.value.serieNumberOfSeasons;
    const newDescription: string = this.editSerieForm.value.serieDescription;
    const newCritique: string = this.editSerieForm.value.serieCritique;
    const newImage: string = this.editSerieForm.value.serieImage;
    //Call the SeriesService's method to update a serie from his id with value 
    //retrieves from form 
    this.seriesService.updateSerie(+this.serie.id, newName, newDate, newNumberOfSeasons,
      newDescription, newCritique, newImage);
    //Redirect to series-view
    this.router.navigate(['/series']);
  }

}
