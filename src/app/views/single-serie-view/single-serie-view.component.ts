import { Component, OnInit } from '@angular/core';
import { Serie } from 'src/app/models/serie.model';
import { SeriesService } from 'src/app/services/series/series.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-serie-view',
  templateUrl: './single-serie-view.component.html',
  styleUrls: ['./single-serie-view.component.css']
})
export class SingleSerieViewComponent implements OnInit {
  serie: Serie;
  commentsUrl: string;
  commentsDisabled: boolean = false;

  constructor(private seriesService: SeriesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    //Retrieve the id parameter in path 
    const id = this.route.snapshot.params.id;
    //Retrieve the serie from id 
    this.serie = this.seriesService.getElementById(+id);
    //Define custom path with the serie's id
    this.commentsUrl = '/serie/' + this.serie.id + '/comments';
    //Check if serie has comments
    //If not => disabled show comments button
    if (this.serie.comments.length === 0) {
      this.commentsDisabled = true;
    }
  }

}
