import { Injectable } from '@angular/core';
import { Serie } from 'src/app/models/serie.model';
import { Comment } from 'src/app/models/comment.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeriesService {
  private _series: Array<Serie> = new Array<Serie>();
  seriesSubject = new Subject();
  serie1: Serie;
  serie2: Serie;
  serie3: Serie;

  constructor() {
    //Creation of series and their comments
    this.serie1 = new Serie(
      'How I Met Your Mother',
      new Date(2005, 8, 19),
      9,
      "Ted se remémore ses jeunes années, lorsqu'il était encore célibataire. Il raconte à ses enfants avec nostalgie ses moments d'égarements et de troubles, ses rencontres et ses recherches effrénées du Grand Amour et les facéties de sa bande d'amis...",
      "Une comédie attachante, « culte » comme on dit, mais qui nous a fait tourner en bourrique trop longtemps pour finir aussi facilement, sur une scène lâchant en toute décontraction une sorte de « ben en fait, la mère on s’en fout un peu. » ",
      'https://media.giphy.com/media/k39w535jFPYrK/giphy.gif');
    ;
    this.serie1.comments.push(new Comment(new Date(2012, 5, 21), 'Claire', 'Trop bien cette série!!'));
    this.serie1.comments.push(new Comment(new Date(2018, 1, 12), 'Charlotte', 'PALALA!! Un vrai régal, du début à la fin.'));
    this.serie1.comments.push(new Comment(new Date(2019, 9, 4), 'Chloé', 'Truc de ouf cette série!!'));
    this.serie1.comments.push(new Comment(new Date(2020, 5, 17), 'KikooDarkLol', 'c t cher drol 7 séri'));

    this.serie2 = new Serie(
      'Kaamelott',
      new Date(2005, 0, 3),
      6,
      "Le quotidien banal et burlesque du roi Arthur et des chevaliers de la Table ronde : quête du Graal, repas en famille et stratégie militaire.",
      "Transgression des codes télévisuels, mélange des genres et des temporalités… Telles furent les recettes du succès de Kaamelott ! Pendant quatre ans, Alexandre Astier a revisité le mythe arthurien et l'a détourné dans un registre comique, sans merci mais avec brio ! Une série qui devrait… un jour… arriver au cinéma !",
      'https://media.giphy.com/media/dfK8oQGInIkj6/giphy.gif');
    this.serie2.comments.push(new Comment(new Date(2015, 5, 23), 'Claire', 'J\'ai beaucoup ri devant devant cette série, mais j\'ai moins aimé le livre VI.'));
    this.serie2.comments.push(new Comment(new Date(2005, 1, 22), 'Charlotte', 'Bien drôle, hâte de voir la suite :)'));
    this.serie2.comments.push(new Comment(new Date(2019, 9, 14), 'Chloé', 'Tellement hâte de voir le film!'));
    this.serie2.comments.push(new Comment(new Date(2020, 5, 27), 'KikooDarkLol', 'je penss c ma séri préféré, en + Astier c 1 lyonnais, aller l\'OL'));

    this.serie3 = new Serie(
      'New girl',
      new Date(2011, 8, 20),
      7,
      "Depuis qu'elle a découvert l'infidélité de son petit ami, Jess, une jeune femme naïve et maladroite, partage un appartement avec trois garçons : Schmidt, Nick et Winston. Cette cohabitation change leur quotidien à tous. Entre leurs déboires sentimentaux respectifs, la petite bande cherche sa place dans le monde.",
      "Lancée en 2011, New Girl est arrivée comme un Ovni naïf et sentimental, sur le petit écran américain. Fait à l'image de son héroïne, cette sitcom douce et loufoque détonait alors par une innocence ostensiblement fleur bleue, un peu ringarde, quasi-ahurie même. À une époque où le cynisme et la dérision étaient plutôt la marque de fabrique des comédies en vogue, New Girl a apporté une voie différente, faisant passer les sentiments au premier plan, et même souvent un certain sentimentalisme.",
      'https://media.giphy.com/media/Dnt2VnWFknFNm/giphy.gif');
    this.serie3.comments.push(new Comment(new Date(2019, 5, 21), 'Claire', 'J\'ai bien aimé!'));
    this.serie3.comments.push(new Comment(new Date(2019, 1, 12), 'Charlotte', 'Pas la série de l\'année, mais ça se regarde quand même.'));
    this.serie3.comments.push(new Comment(new Date(2019, 9, 4), 'Chloé', 'Dommage que la série soit déjà terminée.'));
    this.serie3.comments.push(new Comment(new Date(2019, 5, 17), 'KikooDarkLol', 'c vraimen dla merde 7 séri 1 vré truc 2 meuf. g mem pa regardé 3 épizode'));

    //Push series into private _series array
    this._series.push(this.serie1);
    this._series.push(this.serie2);
    this._series.push(this.serie3);
  }

  //Getters and setters for private _series array
  public get series(): Array<Serie> {
    return this._series;
  }
  public set series(value: Array<Serie>) {
    this._series = value;
  }

  //Observer => update seriesSubject from _series array
  emitSeriesSubject() {
    this.seriesSubject.next(this._series);
  }

  //Add serie to _series array
  addSerie(serie: Serie) {
    this._series.push(serie);
    this.emitSeriesSubject();
  }

  //Delete serie from his id
  deleteSerie(idSerie: number) {
    for (let i = 0; i < this._series.length; i++) {
      if (this._series[i].id === idSerie) {
        this._series.splice(i, 1);
        break;
      }
    }
    this.emitSeriesSubject();
  }

  //Retrieve serie from his id
  getElementById(idSerie: number) {
    for (let serie of this._series) {
      if (serie.id === idSerie) {
        return serie;
      }
    }
  }

  //Update attributes (name, date, nb of seasons, description, critique, image url) of a serie from his id
  updateSerie(idSerie: number, newName: string, newDate: Date, newNumberOfSeasons: number,
    newDescription: string, newCritique: string, newImage: string) {
    for (let serie of this._series) {
      if (serie.id === idSerie) {
        serie.name = newName;
        serie.firstSeasonReleaseDate = newDate;
        serie.numberOfSeasons = newNumberOfSeasons;
        serie.description = newDescription;
        serie.critique = newCritique;
        serie.image = newImage;
      }
    }
    this.emitSeriesSubject();
  }

  //Delete comment of the serie from his id
  deleteComment(idSerie:number, idComment: number) {
    let serie = this.getElementById(idSerie);
      for (let i = 0; i < serie.comments.length; i++) {
        if (serie.comments[i].id === idComment) {
          serie.comments.splice(i, 1);
        }
      }
    this.emitSeriesSubject();
  }

}

