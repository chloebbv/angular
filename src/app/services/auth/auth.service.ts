import { Injectable } from '@angular/core';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAuth: boolean;

  constructor(private userService: UserService) { }

  //Check username and password and establish connection (=> set isAuth to true)
  checkAuth(username: string, password: string) {
    const user = this.userService.user;
    return new Promise(
      (res, rej) => {
        if (user.username === username && user.password === password) {
          this.signIn(res);
        }
        rej('Les identifiants ne sont pas valides. SORRY DUDE!');
      }
    )
  }

  signIn(res) {
    this.isAuth = true;
    res(true);
  }

  //Disconnect (=> set isAuth to false)
  signOut() {
    this.isAuth = false;
  }
}
