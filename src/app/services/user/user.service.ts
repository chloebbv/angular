import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user: User;

  //Instantiate admin user
  constructor() {
    this.user = new User('Administrateur', 'azerty');
  }

  //Getter and setter for private _user
  public get user(): User {
    return this._user;
  }
  public set user(value: User) {
    this._user = value;
  }

}
